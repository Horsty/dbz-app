import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';

import './App.css';
import Cards from './components/Cards/Cards';
import Contact from './components/Contact';
import About from './components/About';
import NavBar from './components/NavBar';
import Home from './components/Home';
import Login from './components/Login/Login';
import Logout from './components/Logout/Logout';

class App extends Component {

  render() {
    return (
      <Router>
        <div className="App">
          <header className="App-header">
            <h1 className="App-title">Welcome to React</h1>
          </header>
          <div>
            <NavBar />
          </div>
          <div className='h-100'>
            <Switch>
              <Route exact path="/home" component={Home} />
              <Route exact path="/contact" component={Contact} />
              <Route exact path="/cards" component={Cards} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/about" component={About} />
              <Route exact path="/logout" component={Logout} />
              <Route exact path="/" render={() => (<Redirect to="/home" />)} />
            </Switch>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;