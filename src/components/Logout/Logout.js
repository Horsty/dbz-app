import React, { Component } from 'react';
// import './Login.css';
import AuthService from '../AuthService/AuthService';
import withAuth from '../AuthService/withAuth';
import Template from '../Template';

class Logout extends Component {
    constructor() {
        super();
        this.Auth = new AuthService();
    }

    handleLogout() {
        this.Auth.logout();
        this.props.history.replace('/login');
    }

    render() {
        const logout = <button type="button" className="form-submit" onClick={this.handleLogout.bind(this)}>Logout</button>
        return (
            <div>
                <Template title="Logout" />
                {logout}
            </div>
        );
    };
}

export default withAuth(Logout);