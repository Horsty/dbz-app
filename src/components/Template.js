import React, { Component } from 'react';

class Template extends Component {
    render() {
        const { title } = this.props;
        return (
            <div>
                <p className="page-info">
                    This is the {title} page.
                </p>
            </div>
        );
    }
}

export default Template;