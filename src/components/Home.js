import React, { Component } from 'react';
import Template from './Template';

class Home extends Component {
    render() {
        return (
            <div className="App-intro">
                <Template title="Home" />
            </div>
        );
    }
}

export default Home;