import React, { Component } from 'react';
import {
    Link
} from 'react-router-dom';
import AuthService from './AuthService/AuthService';

class NavBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            links: [],
            isLoaded: false,
        };
        this.Auth = new AuthService();
    }

    componentDidMount() {
        this.setState({ links: this.props.links, isLoaded: true });
    }

    render() {
        //@TODO export route in routes.js
        let routes = [{
            path: "/home",
            label: "Home"
        }, {
            path: "/contact",
            label: "Contact"
        }, {
            path: "/cards",
            label: "Cards"
        }, {
            path: "/about",
            label: "About"
        }];

        let authRoute = this.Auth.loggedIn() ? {
            path: "/logout",
            label: "Logout"
        } : {
                path: "/login",
                label: "Login"
            };
        routes.push(authRoute);

        return (
            <div className="navbar">
                {routes.map(link => (
                    <Link key={link.path} to={link.path}>
                        {link.label}
                    </Link>
                ))}
            </div>
        );
    }
}

export default NavBar;