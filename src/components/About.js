import React, { Component } from 'react';
import Template from './Template';

class About extends Component {
    render() {
        return (
            <Template title="About" />
        );
    }
}

export default About;