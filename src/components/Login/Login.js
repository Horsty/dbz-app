import React, { Component } from 'react';
import './Login.css';
import AuthService from '../AuthService/AuthService';

class Login extends Component {
    constructor() {
        super();
        this.Auth = new AuthService();
    }

    componentWillMount() {
        if (this.Auth.loggedIn())
            this.props.history.replace('/');
    }

    render() {
        const loginForm = <form onSubmit={this.handleFormSubmit}>
            <input
                className="form-item"
                placeholder="Email goes here..."
                name="email"
                type="text"
                onChange={this.handleChange}
            />
            <input
                className="form-item"
                placeholder="Password goes here..."
                name="password"
                type="password"
                onChange={this.handleChange}
            />
            <input
                className="form-submit"
                value="SUBMIT"
                type="submit"
            />
        </form>
        return (
            <div className="center">
                <div className="card">
                    <h1>Login</h1>
                    { loginForm }
                </div>
            </div>
        );
    }

    // arrow fx for binding
    handleChange = (e) => {
        this.setState(
            {
                [e.target.name]: e.target.value
            }
        )
    }

    // arrow fx for binding
    handleFormSubmit = (e) => {
        e.preventDefault();
        this.Auth.login(this.state.email, this.state.password)
            .then(res => {
                this.props.history.replace('/');
            })
            .catch(err => {
                alert(err);
            })
    }

}

export default Login;