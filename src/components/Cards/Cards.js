import React, { Component } from 'react';
import './Cards.css';
import AuthService from '../AuthService/AuthService';
import Template from '../Template';
import withAuth from '../AuthService/withAuth';

const url = "http://back.horsty.test/api/cards/";

class Cards extends Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
        this.Auth = new AuthService();
    }

    componentDidMount() {
        this.Auth.fetch(url, {
            method: 'GET'
        })
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        const { error, isLoaded, items } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div>
                    <Template title="Cards" />
                    <ul>
                        {items.map(item => (
                            <li key={item.id}>
                                {item.name}
                            </li>
                        ))}
                    </ul>
                </div>
            );
        }
    }
}

export default withAuth(Cards);